﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Guid Set(T entity)
        {
            if (entity.Id == Guid.Empty)
                entity.Id = Guid.NewGuid();
            else if (GetByIdAsync(entity.Id) != null)
                throw new Exception($"Сущность с id {entity.Id} уже существует");

            Data = Data.Concat(new List<T> { entity });

            return GetByIdAsync(entity.Id) is null
                ? throw new Exception("Не удалось добавить сущность")
                : entity.Id;
        }

        public void Remove(Guid id)
        {
            if (GetByIdAsync(id) is null)
                throw new Exception($"Сущность с id {id} не найдена");
            Data = Data.Where(t => t.Id != id);
            
        }

        public Task<T> Update(T entity)
        {
            if (entity.Id == Guid.Empty)
                throw new Exception($"Id сущности пуст");
             if(GetByIdAsync(entity.Id) is null)
                throw new Exception($"Сущность с id {entity.Id} не найдена");

            Remove(entity.Id);
            Set(entity);

            return GetByIdAsync(entity.Id);
        }

    }
}