﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Cоздать сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<EmployeeResponse>> SetEmployee(EmployeeCreateDto newEmployee)
        {
            //Роли и Промокоды предполагается назначать отдельными методами
            var employeeDb = new Employee()
            {
                Email = newEmployee.Email,
                FirstName = newEmployee.FirstName,
                LastName = newEmployee.LastName,
            };

            var empId = _employeeRepository.Set(employeeDb);

            var employeeModel = new EmployeeResponse()
            {
                Id = empId,
                Email = employeeDb.Email,
                Roles = employeeDb.Roles?.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employeeDb.FullName,
                AppliedPromocodesCount = employeeDb.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult> RemoveEmployee(Guid id)
        {
            _employeeRepository.Remove(id);
            return Ok();
        }


        /// <summary>
        /// Редактировать сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost(nameof(UpdateEmployee), Name = nameof(UpdateEmployee))]
        public async Task<ActionResult<EmployeeResponse>> UpdateEmployee(Guid id, EmployeeCreateDto editEmployee)
        {
            //Роли и Промокоды предполагается назначать отдельными методами

            var employeeDb = await _employeeRepository.GetByIdAsync(id);

            employeeDb.Email = editEmployee.Email;
            employeeDb.FirstName = editEmployee.FirstName;
            employeeDb.LastName = editEmployee.LastName;

            var updEmp = await _employeeRepository.Update(employeeDb);

            var employeeModel = new EmployeeResponse()
            {
                Id = updEmp.Id,
                Email = updEmp.Email,
                Roles = updEmp.Roles?.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = updEmp.FullName,
                AppliedPromocodesCount = updEmp.AppliedPromocodesCount
            };

            return employeeModel;
        }
    }
}